#!/bin/sh
#
# This is a shell library for common update/gc related functions
# used by various Girocco scripts.

# shlib.sh always sets this, it's an error to source
# this script without having already sourced shlib.sh
[ -n "$var_git_exec_path" ] || exit 2

pidactive() {
	if _result="$(kill -0 "$1" 2>&1)"; then
		# process exists and we have permission to signal it
		return 0
	fi
	case "$_result" in *"not permitted"*)
		# we do not have permission to signal the process
		return 0
	esac
	# process does not exist
	return 1
}

createlock() {
	# A .lock file should only exist for much less than a second.
	# If we see a stale lock file (> 1h old), remove it and then,
	# just in case, wait 30 seconds for any process whose .lock
	# we might have just removed (it's racy) to finish doing what
	# should take much less than a second to do.
	_stalelock="$(find -L "$1.lock" -maxdepth 1 -mmin +60 -print 2>/dev/null)" || :
	if [ -n "$_stalelock" ]; then
		rm -f "$_stalelock"
		sleep 30
	fi
	for _try in p p n; do
		if (set -C; >"$1.lock") 2>/dev/null; then
			echo "$1.lock"
			return 0
		fi
		# delay and try again
		[ "$_try" != "p" ] || sleep 1
	done
	# cannot create lock file
	return 1
}

# Create a new lockfile
# $1 => name of variable to store result in
# $2 => name of the new lockfile (default is "lockfile.pid")
# $3 => "type" name to use in error message (default is ${2%%.*})
# On success:
#   variable named by $1 will contain the name of the newly create lockfile (i.e. "$2")
# On failure:
#   variable named by $1 will contain the failure reason
v_lock_file() {
	# be compatibile with gc.pid file from newer Git releases
	_lockf="${2:-lockfile.pid}"
	_locktype="${3:-${2%%.*}}"
	_hn="$(hostname)"
	_active=
	if [ "$(createlock "$_lockf")" ]; then
		# If $_lockf is:
		#   1) less than 12 hours old
		#   2) contains two fields (pid hostname) NO trailing NL
		#   3) the hostname is different OR the pid is still alive
		# then we exit as another active process is holding the lock
		if [ "$(find -L "$_lockf" -maxdepth 1 -mmin -720 -print 2>/dev/null)" ]; then
			_apid=
			_ahost=
			read -r _apid _ahost _ajunk <"$_lockf" || :
			if [ "$_apid" ] && [ "$_ahost" ]; then
				if [ "$_ahost" != "$_hn" ] || pidactive "$_apid"; then
					_active=1
				fi
			fi
		fi
	else
		eval "$1="'"unable to create $_lockf.lock file"'
		return 1
	fi
	if [ -n "$_active" ]; then
		rm -f "$_lockf.lock"
		eval "$1="'"$_locktype already running on machine '\''$_ahost'\'' pid '\''$_apid'\''"'
		return 1
	fi
	printf "%s %s" "$$" "$_hn" >"$_lockf.lock"
	chmod 0664 "$_lockf.lock"
	mv -f "$_lockf.lock" "$_lockf"
	eval "$1="'"$_lockf"'
	return 0
}

# duplicate the first file to the name given by the second file making sure that
# the second file appears atomically all-at-once after the copy has been completed
# and does not appear at all if the copy fails (in which case this function fails)
# if the second file already exists this function fails with status 1
# if the file names are the same this function returns immediately with success
# the optional third argument specifies the temp file prefix (default is "packtmp-")
dupe_file() {
	[ "$1" != "$2" ] || return 0
	! [ -e "$2" ] || return 1
	case "$2" in
		*?/?*)	_tmpdir="${2%/*}";;
		*)	_tmpdir=".";;
	esac
	_tmpfile="$(mktemp "${_tmpdir:-.}/${3:-packtmp-}XXXXXX")" || return 1
	cp -fp "$1" "$_tmpfile" || return 1
	mv -f "$_tmpfile" "$2"
}

# rename_pack oldnamepath newnamepath
# note that .keep and .bndl files are left untouched and not moved at all!
rename_pack() {
	[ $# -eq 2 ] && [ "$1" != "$2" ] || {
		echo >&2 "[$proj] incorrect use of rename_pack function"
		exit 1
	}
	# Git assumes that if the destination of the rename already exists
	# that it is, in fact, a copy of the same bytes so silently succeeds
	# without doing anything.  We duplicate that logic here.
	# Git checks for the .idx file first before even trying to use a pack
	# so it should be the last moved and the first removed.
	for ext in pack bitmap idx; do
		[ -f "$1.$ext" ] || continue
		ln "$1.$ext" "$2.$ext" >/dev/null 2>&1 ||
		dupe_file "$1.$ext" "$2.$ext" >/dev/null 2>&1 ||
		[ -f "$2.$ext" ] || {
			echo >&2 "[$proj] unable to move $1.$ext to $2.$ext"
			exit 1
		}
	done
	for ext in idx pack bitmap; do
		rm -f "$1.$ext"
	done
	return 0
}
