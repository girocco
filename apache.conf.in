##  To convert this file to apache.conf using the current Girocco::Config
##  values either do "make" or "make apache.conf" or ./make-apache-conf.sh
##
# This is an example configuration of a virtualhost running Girocco, as set up
# at repo.or.cz; unfortunately, somewhat independent from Girocco::Config.
# It is not essential for Girocco to use a special virtualhost, however.
<VirtualHost *:80>

# ---- BEGIN LINES TO DUPLICATE ----

	ServerName @@httpdnsname@@
	ServerAlias www.@@httpdnsname@@
	ServerAdmin @@admin@@

	# This is the standard "combined" log format modified as follows:
	#    the REMOTE_USER (%u) has double-quotes around it
	#    the received time is shown as [YYYY-mm-dd_HH:MM:SS +hhmm] (almost RFC 3339 format)
	#        -- this is one character shorter than the default but sorts so much better
	#    when the logio_module is present (almost always) the %O value is prefixed with:
	#        %I->  -- <bytes-received-including-request-and-headers>
	#    the first line of the request ("%r") is prefixed with
	#    	 %X%k: -- <connection-status><keepalive-request-num>
	#                 <keepalive-request-num> will be omitted if apache < 2.2.11
	#    these fields are added to the end:
	#        :%{local}p   -- :<actual-server-port>
	#        %Dus         -- <request-time-in-microseconds>
	#        "%o{Content-Range}" -- <outgoing Content-Range header>
	<IfVersion >= 2.2.11>
	LogFormat "%h %l \"%u\" %{[%F_%T %z]}t %X%k:\"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" :%{local}p %Dus \"%{Content-Range}o\"" girocco
	</IfVersion>
	<IfVersion !>= 2.2.11>
	LogFormat "%h %l \"%u\" %{[%F_%T %z]}t %X:\"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" :%{local}p %Dus \"%{Content-Range}o\"" girocco
	</IfVersion>
	<IfModule logio_module>
		# %I and %O are only available with the logio_module
		<IfVersion >= 2.2.11>
		LogFormat "%h %l \"%u\" %{[%F_%T %z]}t %X%k:\"%r\" %>s %I->%O \"%{Referer}i\" \"%{User-Agent}i\" :%{local}p %Dus \"%{Content-Range}o\"" girocco
		</IfVersion>
		<IfVersion !>= 2.2.11>
		LogFormat "%h %l \"%u\" %{[%F_%T %z]}t %X:\"%r\" %>s %I->%O \"%{Referer}i\" \"%{User-Agent}i\" :%{local}p %Dus \"%{Content-Range}o\"" girocco
		</IfVersion>
	</IfModule>

	# If your distribution does not set APACHE_LOG_DIR before
	# starting Apache you will need to edit the next two directives
	ErrorLog "${APACHE_LOG_DIR}/@@nickname@@-error.log"
	CustomLog "${APACHE_LOG_DIR}/@@nickname@@-access.log" girocco

	<IfModule mime_magic_module>
		# Avoid spurious Content-Type values when git-http-backend
		# fails to provide a Content-Type header in its output
		MimeMagicFile /dev/null
	</IfModule>

	DocumentRoot @@webroot@@
	<Directory @@webroot@@>
		# Add MultiViews only if pages are truly
		# offered in more than a single language
		# FollowSymLinks or SymLinksIfOwnerMatch is required for .htaccess files
		Options FollowSymLinks
		# FileInfo (or All) must be enabled to activate .htaccess file mod_rewrite rules
		AllowOverride All
		<IfVersion < 2.3>
		Order allow,deny
		Allow from all
		Satisfy all
		</IfVersion>
		<IfVersion >= 2.3>
		Require all granted
		</IfVersion>
		DirectoryIndex w
	</Directory>

	# The non-mod_rewrite items are handled first where the magic /[bchrw]
	# prefix always forces selection of the prefix-indicated cgi handler.

	ScriptAlias /w @@cgiroot@@/gitweb.cgi
	ScriptAlias /b @@cgiroot@@/bundles.cgi
	AliasMatch ^/h/(.*\.html)$ @@cgiroot@@/html/$1
	ScriptAliasMatch ^/(?!(?i)gitweb\.cgi|bundles\.cgi|html\.cgi(?:/|$))([^/]+\.cgi(?:/.*)?)$ @@cgiroot@@/$1

	# Any requests without the magic /[bchrw] are treated as Git requests if they
	# are one of the few possible Git URLs otherwise they go to bundles or gitweb

	# Change the setting of $SmartHTTPOnly in Girocco::Config.pm to
	# change whether or not non-smart HTTP fetch access will be allowed.

	<IfDefine !@@SmartHTTPOnly@@>
	# This accelerates non-smart HTTP access to loose objects, packs and info
	AliasMatch \
		"(?x)^/(?![bchw]/)(?:r/)? \
		((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?)(?:\.git)?/( \
			HEAD | \
			objects/info/alternates | \
			objects/info/http-alternates | \
			objects/info/packs | \
			objects/[0-9a-f]{2}/[0-9a-f]{38} | \
			objects/pack/pack-[0-9a-f]{40}\.(?:pack|idx) )$" \
		@@reporoot@@/$1.git/$2
	</IfDefine>

	# SetEnv GIT_HTTP_BACKEND_BIN to override Config.pm $git_http_backend_bin
	ScriptAlias /r/ @@basedir@@/bin/git-http-backend-verify/

	ScriptAliasMatch \
		"(?x)^/(?![bchrw]/) \
		((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?)(?:\.git)?/( \
			info/refs | \
			git-upload-pack | \
			git-receive-pack | \
			[a-zA-Z0-9][a-zA-Z0-9+._-]*\.bundle )$" \
		@@basedir@@/bin/git-http-backend-verify/$1.git/$2

	# Everything else off to bundles.cgi or gitweb.cgi
	ScriptAliasMatch \
		"(?x)^/(?![bchrw]/) \
		((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?\.git/bundles)$" \
		@@cgiroot@@/bundles.cgi/$1
	ScriptAliasMatch \
		"(?x)^/(?![bchrw]/) \
		((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?\.git(?!/bundles)(?:/.*)?)$" \
		@@cgiroot@@/gitweb.cgi/$1

	# mod_rewrite is not strictly required for gitweb and fetch access, but
	# if it's not available the trailing ".git" is never optional for
	# gitweb, the leading /h is always required for *.html, snapshots are
	# not throttled, some bogus Git http protocol requests will not be
	# detected early and, if non-smart HTTP is allowed, access to the
	# /info/refs file will not be accelerated in non-smart HTTP mode.

	<IfModule rewrite_module>
		RewriteEngine On

		# Snapshot/blob_plain requests are only allowed via the PATH_INFO mechanism
		RewriteCond %{QUERY_STRING}	(^|[&;])a=(?:snapshot|blob_plain)([&;]|$) [NC]
		RewriteRule .? - [NS,F,L]

		# Redirect snapshot requests to snapshot.cgi
		RewriteRule \
			"(?x)^/(?![bchr]/)(?:w/)? \
			((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?\.git/ \
				snapshot(?:/.*)?)$" \
			@@cgiroot@@/snapshot.cgi/$1 [NS,L,H=cgi-script]

		# Detect blob_plain requests with is_blob_plain
		RewriteRule \
			"(?x)^/(?![bchr]/)(?:w/)? \
			((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?\.git)/ \
				blob_plain(?:/.*)?$" \
			- [E=is_blob_plain:$1]

		# Reject blob_plain requests if .no_blob_plain file exists and is not zero bytes
		RewriteCond "%{ENV:is_blob_plain}" !=""
		RewriteCond "@@reporoot@@/%{ENV:is_blob_plain}/.no_blob_plain" -s
		RewriteRule ^ - [NS,F]

		# Reject blob_plain requests if .no_blob_plain file exists AND mismatched Referer
		# We require the referer host and port and git project to match the current request
		RewriteCond "%{ENV:is_blob_plain}" !=""
		RewriteCond "@@reporoot@@/%{ENV:is_blob_plain}/.no_blob_plain" -f
		RewriteRule ^ - [C,E=is_blob_ref:1]
		RewriteRule ^ - [C,E=ref_host:]
		RewriteRule ^ - [E=ref_path:]
		RewriteCond "%{ENV:is_blob_ref}" =1
		RewriteCond "%{HTTP_REFERER}" "^https?://(?:[^@/]*@)?([^@:/?#]+(?::[0-9]+)?)"
		RewriteRule ^ - [E=ref_host:%1]
		RewriteCond "%{ENV:is_blob_ref}" =1
		RewriteCond "%{HTTP_REFERER}" "^https?://(?:[^@/]*@)?[^@:/?#]+(?::[0-9]*)?(?:/w)?(.*)$"
		RewriteRule ^ - [E=ref_path:%1]
		RewriteCond "%{ENV:is_blob_ref}" =1
		RewriteCond "@%{HTTP_HOST}=%{ENV:ref_host}@" "!^@([^=]*)=\1@$" [NC,OR]
		RewriteCond "@/%{ENV:is_blob_plain}=%{ENV:ref_path}" "!^@([^=]*)=\1(?:[/?#]|$)"
		RewriteRule ^ - [NS,F]

		# Make the leading /h optional for requests that name an existing .html template
		RewriteCond @@webroot@@/$1 !-f
		RewriteCond @@cgiroot@@/$1 !-f
		RewriteCond @@cgiroot@@/html/$1 -s
		RewriteRule \
			^/(?![bchrw]/)(.*\.html)$ \
			/h/$1 [NS,PT]

		# Redirect bare gitweb requests without .git that name an existing repo...
		RewriteCond @@webroot@@/$2 !-f
		RewriteCond @@cgiroot@@/$2 !-f
		RewriteCond @@reporoot@@/$2.git/HEAD -s
		RewriteRule \
			"(?x)^/(?![bchr]/)((?:w/)?) \
			((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git))$" \
			/$1$2.git [NS,L,R=301]

		# Of the 11 possible Git protocol URLs (i.e. passed to git-http-backend-verify),
		# 9 are only valid with GET/HEAD and the other two are only valid with POST
		# Furthermore, 7 are only valid when non-smart is allowed and
		# 1 is only valid when smart-only is enabled if it has the correct query string.

		# These two always require POST
		RewriteCond %{REQUEST_METHOD} !=POST
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			(?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?(?:\.git)?/(?: \
				git-upload-pack | \
				git-receive-pack )$" \
			- [NS,F]

		<IfDefine @@SmartHTTPOnly@@>
		# These 7 are always forbidden when non-smart HTTP is disabled
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			(?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?(?:\.git)?/(?: \
				HEAD | \
				objects/info/alternates | \
				objects/info/http-alternates | \
				objects/info/packs | \
				objects/[0-9a-f]{2}/[0-9a-f]{38} | \
				objects/pack/pack-[0-9a-f]{40}\.(?:pack|idx) )$" \
			- [NS,F]
		# This one is forbidden without the magic query string when non-smart is disabled
		RewriteCond %{REQUEST_METHOD} !^(?:GET|HEAD)$ [OR]
		RewriteCond %{QUERY_STRING} !(^|&)service=git-(?:upload|receive)-pack(&|$)
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			(?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?(?:\.git)?/ \
				info/refs $" \
			- [NS,F]
		# This one requires GET (or HEAD)
		RewriteCond %{REQUEST_METHOD} !^(?:GET|HEAD)$
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			(?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?(?:\.git)?/ \
				[a-zA-Z0-9][a-zA-Z0-9+._-]*\.bundle $" \
			- [NS,F]
		</IfDefine>

		<IfDefine !@@SmartHTTPOnly@@>
		# These 9 require GET (or HEAD)
		RewriteCond %{REQUEST_METHOD} !^(?:GET|HEAD)$
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			(?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?(?:\.git)?/(?: \
				HEAD | \
				info/refs | \
				objects/info/alternates | \
				objects/info/http-alternates | \
				objects/info/packs | \
				objects/[0-9a-f]{2}/[0-9a-f]{38} | \
				objects/pack/pack-[0-9a-f]{40}\.(?:pack|idx) | \
				[a-zA-Z0-9][a-zA-Z0-9+._-]*\.bundle )$" \
			- [NS,F]
		# This one can be accelerated when accessed with non-smart HTTP
		RewriteCond %{REQUEST_METHOD} ^(?:GET|HEAD)$
		RewriteCond %{QUERY_STRING} !(^|&)service=git-(?:upload|receive)-pack(&|$)
		RewriteRule \
			"(?x)^/(?![bchw]/)(?:r/)? \
			((?:[a-zA-Z0-9][a-zA-Z0-9+._-]*(?<!\.git)/)*[a-zA-Z0-9][a-zA-Z0-9+._-]*?)(?:\.git)?/ \
				info/refs $" \
			@@reporoot@@/$1.git/info/refs [NS,L]
		</IfDefine>
	</IfModule>

	<Directory @@reporoot@@>
		Options FollowSymLinks
		AllowOverride None
		<IfVersion < 2.3>
		Order allow,deny
		Allow from all
		Satisfy all
		</IfVersion>
		<IfVersion >= 2.3>
		Require all granted
		</IfVersion>

		<IfModule rewrite_module>
			# Everything fetched over the non-smart git http
			# protocol should be an existing file.  If the request
			# is not for an existing file, just send back an error
			# message without emitting anything into the error log.
			RewriteEngine On
			RewriteBase /
			RewriteCond @@reporoot@@/$1 !-f
			RewriteRule ^(.*)$ - [NS,R=404,L]
		</IfModule>
	</Directory>

	<Directory @@cgiroot@@>
		# FollowSymLinks or SymLinksIfOwnerMatch is required for .htaccess files
		Options SymLinksIfOwnerMatch
		# FileInfo must be enabled to activate .htaccess file mod_rewrite rules
		AllowOverride FileInfo
		<IfVersion < 2.3>
		Order deny,allow
		Deny from all
		Satisfy all
		</IfVersion>
		<IfVersion >= 2.3>
		Require all denied
		</IfVersion>
		<Files gitweb.cgi>
			Options +ExecCGI
			<IfVersion < 2.3>
			Order deny,allow
			Allow from all
			Satisfy all
			</IfVersion>
			<IfVersion >= 2.3>
			Require all granted
			</IfVersion>
			<IfModule !mod_fastcgi.c>
			<IfModule !mod_fcgid.c>
				SetHandler cgi-script
			</IfModule>
			</IfModule>

			# Note that in testing mod_fastcgi (in dynamic mode)
			# was found to be slightly faster than mod_fcgid.
			#
			# However, we prefer mod_fcgid if both are available
			# because we cannot control the server-global settings
			# of mod_fastcgi's "FastCgiConfig" options.
			#
			# In order for gitweb.cgi to run reasonably well as a
			# mod_fastcgi dynamic FastCGI application, the
			# "FastCgiConfig" option "-idle-timeout" value needs to
			# be increased from the default value of "30" to at
			# least "120", preferably more like "300".  But that
			# will affect ALL dynamic mod_fastcgi applications on
			# the ENTIRE server, not just gitweb.cgi.  Additionally
			# the "FastCgiConfig" "-restart" option probably ought
			# to be set as well.  Also, unfortunately, there is no
			# mod_fastcgi option corresponding to mod_fcgid's
			# MaxRequestsPerProcess option and gitweb.cgi running
			# in FastCGI mode (without using FCGI::ProcManager) will
			# always exit after serving 100 requests (a good thing).
			#
			# The alternative is to make gitweb.cgi a static
			# mod_fastcgi application (the "FastCgiServer"
			# directive), but then the number of running instances
			# will be fixed at whatever value is chosen for the
			# "-processes" option rather than being dynamically
			# adjusted based on load and that's probably undesirable
			# in most cases unless you run gitweb.cgi under a
			# front-end that dynamically forks multiple copies of
			# gitweb.cgi based on the current load.  See the CPAN
			# FCGI::ProcManager::Dynamic module for an example of
			# how to do this in Perl:
			#
			#   http://search.cpan.org/search?query=FCGI::ProcManager::Dynamic&mode=module
			#
			# So instead we prefer mod_fcgid because we can adjust
			# the necessary options for good gitweb.cgi behavior
			# while affecting only gitweb.cgi and having it remain
			# a dynamic application whose total number of running
			# instances is adjusted based on current server load.

			<IfModule mod_fcgid.c>
				SetHandler fcgid-script
			</IfModule>
			<IfModule !mod_fcgid.c>
			<IfModule mod_fastcgi.c>
				SetHandler fastcgi-script
			</IfModule>
			</IfModule>
		</Files>
		<FilesMatch ^(?!(?i)gitweb\.cgi$).*\.cgi$>
			Options +ExecCGI
			SetHandler cgi-script
			<IfVersion < 2.3>
			Order deny,allow
			Allow from all
			Satisfy all
			</IfVersion>
			<IfVersion >= 2.3>
			Require all granted
			</IfVersion>
		</FilesMatch>
	</Directory>
	<Directory @@cgiroot@@/html>
		<IfVersion < 2.3>
		Order deny,allow
		Allow from all
		Satisfy all
		</IfVersion>
		<IfVersion >= 2.3>
		Require all granted
		</IfVersion>
		<Files *.html>
			ForceType "text/html; charset=utf-8"
		</Files>
	</Directory>

	<IfModule mod_fcgid.c>
		# mod_fcgid benefits from some additional config for gitweb.cgi
		# gitweb.cgi has a hard-coded maximum of 100 requests
		# and we do not want to give up too soon in case Git is lagging.
		# Note that adding a 'MaxProcesses ...' option here may be valuable
		# to limit the maximum number of gitweb.cgi processes that can be
		# spawned (default is 100) -- perhaps to something much lower such
		# as 1 or 2 times the number of CPU cores.  Also note that in the
		# unlikely event all the children finish their 100 requests at the
		# same time, the server's FcgidSpawnScoreUpLimit (which defaults
		# to 10 if not set) should be set to at least 3 times the
		# MaxProcesses value chosen to allow them all to respawn
		# immediately.  FcgidSpawnScoreUpLimit MUST be at least twice the
		# chosen MaxProcesses value (assuming FcgidTerminationScore is
		# still set to the default 2) in order to allow any child at all to
		# respawn immediately in this case without a delay.
		FcgidCmdOptions @@cgiroot@@/gitweb.cgi \
		MaxRequestsPerProcess 100 IOTimeout 300
	</IfModule>

	<Directory @@basedir@@/bin>
		Options None
		AllowOverride None
		<IfVersion < 2.3>
		Order deny,allow
		Deny from all
		Satisfy all
		</IfVersion>
		<IfVersion >= 2.3>
		Require all denied
		</IfVersion>
		<Files git-http-backend-verify>
			Options ExecCGI
			SetHandler cgi-script
			<IfVersion < 2.3>
			Order deny,allow
			Allow from all
			Satisfy all
			</IfVersion>
			<IfVersion >= 2.3>
			Require all granted
			</IfVersion>
		</Files>
	</Directory>

# ---- END LINES TO DUPLICATE ----

</VirtualHost>


# Change the setting of $TLSHost in Girocco::Config.pm to change
# whether or not the following https virtual host is enabled.

<IfDefine @@TLSHost@@>

# This is an example configuration of an https virtualhost running Girocco, as set
# up at repo.or.cz; unfortunately, completely independent from Girocco::Config.
# It is not essential for Girocco to use a special virtualhost, however.
# The Config.pm $httpspushurl variable needs to be defined to properly enable
# https pushing.
<VirtualHost *:443>

	# These certificate files will all be automatically generated, but the
	# paths here may need to be corrected to match the paths
	# (especially $certsdir) from Config.pm

	SSLCertificateFile @@certsdir@@/girocco_www_crt.pem
	SSLCertificateKeyFile @@certsdir@@/girocco_www_key.pem
	SSLCertificateChainFile @@certsdir@@/girocco_www_chain.pem
	# When using a www server cert signed by a pre-trusted root, only
	# the above three lines should be changed.  Changing either of the
	# below two lines will likely break https client authentication.
	SSLCACertificateFile @@certsdir@@/girocco_root_crt.pem
	SSLCADNRequestFile @@certsdir@@/girocco_client_crt.pem

	SSLVerifyDepth 3
	SSLOptions +FakeBasicAuth +StrictRequire
	SSLEngine on

	# This configuration allows fetching over https without a certificate
	# while always requiring a certificate for pushing over https
	RewriteEngine On
	SSLVerifyClient optional
	RewriteCond %{REQUEST_METHOD} ^(GET|HEAD)$ [NC]
	RewriteCond %{QUERY_STRING} (^|&)service=git-receive-pack(&|$) [NC]
	RewriteRule /info/refs$ - [NC,NS,env=client_auth_required:1]
	RewriteCond %{REQUEST_METHOD} =POST [NC]
	RewriteRule /git-receive-pack$ - [NC,NS,env=client_auth_required:1]
	RewriteCond %{ENV:client_auth_required} 1
	RewriteCond %{SSL:SSL_CLIENT_VERIFY} !^SUCCESS$
	RewriteRule .? %{REQUEST_URI} [NS,R=401]
	<Location />
		SSLRequireSSL
		SSLOptions +FakeBasicAuth
		AuthName "Git Client Authentication"
		AuthType Basic
		AuthBasicProvider anon
		Anonymous *
		<IfVersion < 2.3>
		Order deny,allow
		Deny from env=client_auth_required
		Satisfy any
		Require valid-user
		</IfVersion>
		<IfVersion >= 2.3>
		<RequireAny>
		<RequireAll>
		Require all granted
		Require not env client_auth_required
		</RequireAll>
		Require valid-user
		</RequireAny>
		</IfVersion>
	</Location>
	ErrorDocument 401 /authrequired.cgi

# ---- BEGIN DUPLICATE LINES ----
##
##  *** IMPORTANT ***
##
##  ALL the entire contents from the <VirtualHost *:80> section at the top of
##  this file must be copied here.
##
##  To avoid this duplication, the contents of the <VirtualHost *:80> section
##  above can be moved to a separate file and then included both here and in
##  the <VirtualHost *:80> section using an Include directive.  Be careful not
##  to place the new include file in one of the directories the standard apache
##  configuration blindly includes all files from.
##
# ---- END DUPLICATE LINES ----

</VirtualHost>

</IfDefine>
