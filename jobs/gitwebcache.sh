#!/bin/sh

. @basedir@/shlib.sh

set -e

LOCK_FILE="/tmp/gitwebcache-$cfg_tmpsuffix.lock"

# Make sure we don't run twice.
if [ -s "$LOCK_FILE" ] && kill -0 "$(cat "$LOCK_FILE")" 2>/dev/null; then
	echo "Already running gitwebcache.sh (stuck?) with pid $(cat "$LOCK_FILE")" >&2
	exit 1
fi
trap 'rm -f "$LOCK_FILE"' EXIT
trap 'exit 130' INT
trap 'exit 143' TERM
echo $$ >"$LOCK_FILE"

cd "$cfg_cgiroot"

# Re-generate the cache; we must be in same group as cgi user and
# $cache_grpshared must be 1.
# We get rid even of stderr since it's just junk from broken repos.
# We must set REQUEST_METHOD or gitweb.cgi will bail early
REQUEST_METHOD=HEAD && export REQUEST_METHOD
perl -e 'do "./gitweb.cgi"; END {
	fill_project_list_info([], "rebuild-cache") }' >/dev/null 2>&1

rm -f "$LOCK_FILE"
trap - EXIT INT TERM
