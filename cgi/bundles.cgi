#!/usr/bin/perl

# bundles.cgi -- support for viewing a project's downloadable bundles
# Copyright (c) 2015 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;
use POSIX qw(strftime);
binmode STDOUT, ':utf8';

# Never refresh more often than this
my $min_refresh = 120;

# Extract the project name, we prefer PATH_INFO but will use a name= param
my $projname = '';
if ($ENV{'PATH_INFO'}) {
	$projname = $ENV{'PATH_INFO'};
	$projname =~ s|/+$||;
	$projname =~ s|/bundles$||;
}
if (!$projname && $ENV{'QUERY_STRING'}) {
	if ("&$ENV{'QUERY_STRING'}&" =~ /\&name=([^&]+)\&/) {
		$projname = $1;
	}
}
$projname =~ s|/+$||;
$projname =~ s|\.git$||i;
$projname =~ s|^/+||;

my $gcgi = undef;

sub prefail {
	$gcgi = Girocco::CGI->new('Project Bundles')
		unless $gcgi;
}

# Do we have a project name?

if (!$projname) {
	prefail;
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

# Do we have a valid, existing project name?

if (!Girocco::Project::does_exist($projname, 1)) {
	prefail;
	if (Girocco::Project::valid_name($projname)) {
		print "<p>Sorry but the project $projname does not exist.  " .
			"Now, how did you <em>get</em> here?!</p>\n";
	} else {
		print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	}
	exit;
}

# Load the project and possibly parent projects

my $proj = Girocco::Project->load($projname);
if (!$proj) {
	prefail;
        print "<p>not found project $projname, that's really weird!</p>\n";
        exit;
}
my @projs = ($proj);
my $parent = $projname;
# Walk up the parent projects loading each one that exists until we
# find a bundle or we've loaded all parents
while (!$projs[0]->has_bundle && $parent =~ m|^(.*[^/])/[^/]+$|) {
	$parent = $1;
	# It's okay if some parent(s) do not exist because we may simply have
	# a grouping without any forking going on
	next unless Girocco::Project::does_exist($parent, 1);
	my $pproj = Girocco::Project->load($parent);
	next unless $pproj;
	unshift(@projs, $pproj);
}

# At this point we produce different output depending on whether or not
# we actually found a bundle.

# We also select a refresh time based on when we expect the bundle to be
# replaced (if we found one) or when we expect one to be created (if we didn't)
# If we found a bundle, it will be from $projs[0].

# We currently ignore all but the most recent bundle for a project.

my $got_bundle = $projs[0]->has_bundle;
my $now = time;
my @bundle = ();
my @nextgc = $projs[0]->next_gc;
my $willgc = $projs[0]->needs_gc;
my $expires = undef; # undef = unknown, 0 = expired
my $behind = undef; # only meaningful if we've got a bundle, undef = unknown, 0 = current
my $refresh = undef;
my $isempty = undef;
my $inprogress = undef;
if ($got_bundle) {
	$isempty = 0;
	@bundle = @{($projs[0]->bundles)[0]};
	my $lastgc = parse_any_date($projs[0]->{lastgc});
	$bundle[0] = $lastgc if defined($lastgc) && $lastgc > $bundle[0];
	if (defined($nextgc[0])) {
		$expires = $nextgc[0] - $now;
		$expires = 0 if $expires < 0;
	} else {
		$expires = 0 if $willgc;
	}
	if (defined($expires)) {
		# Refresh is half of expires time
		$refresh = int($expires / 2);
	} else {
		# we have a bundle, but for some reason we have no idea when
		# it will expire, so refresh after 12 hours
		$refresh = 12 * 3600;
	}
	my $lastch = parse_any_date($projs[0]->{lastchange});
	if (defined($lastch)) {
		$behind = $lastch - $bundle[0];
		$behind = 0 if $behind < 0;
	}
} else {
	# Project could be:
	#  1) empty -- no guess about when not "empty"
	#  2) building one now "building"
	#  3) $nextgc[1] if defined
	#  4) "not available"
	if ($projs[0]->is_empty) {
		$isempty = 1;
		# No idea when something will be pushed so use 8 hours
		$refresh = 8 * 3600;
	} elsif ($projs[0]->{gc_in_progress}) {
		$inprogress = 1;
		$expires = 0;
		# Building now, use the minimum refresh
		$refresh = $min_refresh;
	}  elsif (defined($nextgc[1])) {
		# in this case expires indicates when we expect a bundle
		# and we use 'Expected' instead of 'Expires'
		$expires = $nextgc[1] - $now;
		$expires = 0 if $expires < 0;
		# use half the time
		$refresh = int($expires / 2);
	} else {
		if ($willgc) {
			$expires = 0 if $willgc;
			$refresh = $min_refresh;
		} else {
			# else not available
			# Make the refresh 16 hours
			$refresh = 16 * 3600;
		}
	}
}

my $eh = undef;
$refresh = $min_refresh if defined($refresh) && $refresh < $min_refresh;
$eh = "<meta http-equiv=\"refresh\" content=\"$refresh\" />\n"
	if defined($refresh) && !$got_bundle; # do not refresh away instructions

my $projlink = url_path($Girocco::Config::gitweburl).'/'.$projname.'.git';
$gcgi = Girocco::CGI->new('bundles', $projname.'.git', $eh, $projlink);

print "<p>Downloadable Git bundle information for project <a href=\"$projlink\">".
	"$projname</a> as of @{[strftime('%Y-%m-%d %H:%M:%S UTC',
	(gmtime($now))[0..5], -1, -1, -1)]}:</p>\n";

sub format_th {
	my ($title, $explain) = @_;
	return $explain ?
		"<span class=\"hover\">$title<span><span class=\"head\" _data=\"$title\"></span>".
			"<span class=\"none\" /><br />(</span>$explain<span class=\"none\">)</span></span></span>" :
		$title;
}

sub rel_time {
	my $s = shift || 0;
	return "0" unless $s;
	return "1 second" if $s < 2;
	return $s . " seconds" if $s < 120;
	$s = int(($s + 30) / 60);
	return $s . " minutes" if $s < 120;
	$s = int(($s + 30) / 60);
	return $s . " hours" if $s < 48;
	$s = int(($s + 12) / 24);
	return $s . " days" if $s < 14;
	$s = int(($s + 3.5) / 7);
	return $s . " weeks" if $s < 9;
	$s = int(($s * 7 + 15.25) / 30.5);
	return $s . " months" if $s < 24;
	$s = int(($s + 6) / 12);
	return $s . " years";
}

sub _nbsp {
	my $text = shift;
	$text =~ s/ /&#160;/g;
	return $text;
}

sub expires_string {
	my $expires = shift;
	return "unknown" unless defined($expires);
	return "any moment" unless $expires > 0;
	return _nbsp(rel_time($expires));
}

sub behind_string {
	my $behind = shift;
	return "unknown" unless defined($behind);
	return "current" unless $behind > 0;
	return _nbsp(rel_time($behind));
}

sub extra_string {
	my $extra = shift;
	return '' if !defined($extra) || $extra !~ /^\d+$/;
	return "empty" if !$extra;
	return _nbsp('+'.human_size($extra * 1024));
}

sub sizek_string {
	my $sizek = shift;
	return "unknown" if !defined($sizek) || $sizek !~ /^\d+$/;
	return "empty" if !$sizek;
	return _nbsp(human_size($sizek * 1024));
}

my ($ex_title, $ex_explain) = $got_bundle ?
	("Expires", "Time remaining before bundle may become unavailable"):
	("Expected", "Time remaining until a bundle is generated");

print <<EOT;
<table class='bundlelist'><tr valign="top" align="left"><th>Project</th
><th>@{[format_th("Bundle", "Downloadable git bundle")]}</th
><th>Size</th
><th>@{[format_th($ex_title, $ex_explain)]}</th
><th>@{[format_th("Behind", "Time since bundle creation until most recently received ref change")]}</th
></tr>
EOT

my $plink = url_path($Girocco::Config::gitweburl).'/'.$projs[0]->{name}.'.git';
print "<tr valign=\"top\" class=\"odd\"><td><a href=\"$plink\">$projs[0]->{name}</a></td>";
my $blink;
if ($got_bundle) {
	# Git yer bundle here
	$blink = url_path($Girocco::Config::httpbundleurl).'/'.$projs[0]->{name}.'.git/'.$bundle[1];
	print "<td><a rel='nofollow' href=\"$blink\">$bundle[1]</a></td>".
		"<td>@{[_nbsp(human_size($bundle[2]))]}</td>".
		"<td>@{[expires_string($expires)]}</td>".
		"<td>@{[behind_string($behind)]}</td></tr>\n";
} else {
	print "<td>@{[$inprogress&&!$isempty?'building':'']}</td><td>".
		_nbsp(sizek_string($isempty?0:$projs[0]->{reposizek})).
		"</td><td>@{[expires_string($expires)]}</td><td></td></tr>\n";
}
my $extrasizek = 0;
for (my $i=1; $i <= $#projs; ++$i) {
	print "<tr";
	print " class=\"odd\"" unless $i % 2;
	$plink = url_path($Girocco::Config::gitweburl).'/'.$projs[$i]->{name}.'.git';
	my $pname = $projs[$i]->{name};
	$pname =~ s|^.*[^/]/||;
	print "><td><span style=\"display:inline-block;height:1em;width:@{[2*($i-1)+1]}ex\"></span>".
		"&#x2026;/<a href=\"$plink\">$pname</a></td><td></td><td>";
	my $rsk = $projs[$i]->{reposizek};
	$rsk = undef unless $rsk =~ /^\d+$/;
	$rsk = 0 if !defined($rsk) && $projs[$i]->is_empty;
	$extrasizek = defined($rsk) ? $extrasizek + $rsk : undef if defined($extrasizek);
	print extra_string($extrasizek) if $i == $#projs;
	print "</td><td></td><td>";
	if ($got_bundle && $i == $#projs) {
		my $lch = parse_any_date($projs[$i]->{lastchange});
		my $bh = undef;
		if (defined($lch)) {
			$bh = $lch - $bundle[0];
			$bh = 0 if $bh < 0;
		}
		print behind_string($bh);
	}
	print "</td></tr>\n";
}
print "</table>\n";

if (!$got_bundle) {
	print <<EOT;
<p>At this time there is no Git downloadable bundle available for 
project <a href="$projlink">$projname</a>.</p>
<p>You may want to check back later based on the information shown above.</p>
EOT
	exit 0;
}

if ($#projs) {
	print <<EOT;
<p>Although there is no Git downloadable bundle available for 
project <a href="$projlink">$projname</a>, since it is a fork of
project <a href="$plink">$projs[0]->{name}</a> which <em>does</em>
have a bundle, that bundle can be used instead which will reduce the
amount that needs to be fetched with <tt>git fetch</tt> to only those
items that are unique to the project $projname fork.</p>
EOT
}

my $projbase = $projname;
$projbase =~ s|^.*[^/]/||;
my $fetchurl = $Girocco::Config::httppullurl;
$fetchurl = $Girocco::Config::httpbundleurl unless $fetchurl;
$fetchurl = $Girocco::Config::gitpullurl unless $fetchurl;
$fetchurl .= "/".$projname.".git";
my $forkchanges = '';
my $forksize = '';
if ($#projs) {
	$forkchanges = " specific to the fork or";
	$forksize = " and how different the fork is from its parent";
}

print <<EOT;
<div class="htmlcgi">

<h3>Instructions</h3>

<h4>0. Quick Overview</h4>
<div>
<ol>
<li>Download the bundle (possibly resuming the download if interrupted) using any available technique.
</li>
<li>Create a repository from the bundle.
</li>
<li>Reset the repository&#x2019;s origin to a fetch URL.
</li>
<li>Fetch the latest changes and (optionally) the current HEAD symbolic ref.
</li>
<li>Select a desired branch and check it out.
</li>
</ol>
</div>

<h4>1. Download the Bundle</h4>
<div class="indent">
<p>Download the <a rel='nofollow' href="$blink">$bundle[1]</a> file using your favorite method.</p>
<p>Web browsers typically provide one-click pause and resume.  The <tt>curl</tt> command line
utility has a <tt>--continue-at</tt> option that can be used to resume an interrupted download.</p>
<p><em>Please note that it may not be possible to resume an interrupted download after the
&#x201c;Expires&#x201d; time shown above so plan the bundle download accordingly.</em></p>
<p>Subsequent instructions will assume the downloaded bundle <tt>$bundle[1]</tt> is available in
the current directory &#x2013; adjust them if that&#x2019;s not the case.</p>
</div>

<h4>2. Create a Repository from the Bundle</h4>
<div class="indent">
<p>It is possible to use the <tt>git clone</tt> command to create a repository
from a bundle file all in one step.  However, that can result in unwanted local
tracking branches being created, so we do not use <tt>git clone</tt> in this
example.</p>
<p>This example creates a Git repository named &#x201c;<tt>$projbase</tt>&#x201d;
in the current directory, but that may be adjusted as desired:</p>
<pre class="indent">
git init $projbase
cd $projbase
git remote add origin ../$bundle[1]
git fetch
</pre>
</div>

<h4>3. Reset the Origin</h4>
<div class="indent">
<p>Assuming the current directory is still set to the newly created
&#x201c;<tt>$projbase</tt>&#x201d; repository, we set the origin to
a suitable fetch URL.  Any valid fetch URL for the repository may be used
instead of the one shown here:</p>
<pre class="indent">
git remote set-url origin $fetchurl
</pre>
<p>Note that the $bundle[1] file is now no longer needed and may be kept or
discarded as desired.</p>
</div>

<h4>4. Fetch Updates</h4>
<div class="indent">
<p>Assuming the current directory is still set to the newly created
&#x201c;<tt>$projbase</tt>&#x201d; repository, this example fetches
the current <tt>HEAD</tt> symbolic ref (i.e. the branch that would
be checked out by default if the repository had been cloned directly
from a fetch URL instead of a bundle) and any changes$forkchanges made
to the repository since the bundle was created:</p>
<pre class="indent">
git fetch --prune origin
git remote set-head origin --auto
</pre>
<p>The amount retrieved by the <tt>fetch</tt> command depends on how many changes
have been pushed to the repository since the bundle was created$forksize.</p>
<p>The <tt>set-head</tt> command will be very fast and may be omitted if one&#x2019;s
not interested in the repository&#x2019;s default branch.</p>
</div>

<h4>5. Checkout</h4>
<div class="indent">
<p>Assuming the current directory is still set to the newly created
&#x201c;<tt>$projbase</tt>&#x201d; repository, the list of available
branches to checkout may be shown like so:</p>
<pre class="indent">
git branch -r
</pre>
<p>Note that if the repository has a default branch it will be shown in the
listing preceded by &#x201c;<tt>origin/HEAD -> </tt>&#x201d;.</p>
<p>In this case, however, the default branch is most likely
&#x201c;<tt>$projs[$#projs]->{HEAD}</tt>&#x201d; and may be checked out like so:</p>
<pre class="indent">
git checkout $projs[$#projs]->{HEAD}
</pre>
<p>Note that the leading &#x201c;<tt>origin/</tt>&#x201d; was omitted from the
branch name given to the <tt>git checkout</tt> command so that the automagic
DWIM (Do What I Mean) logic kicks in.</p>
<p>The repository is now ready to be used just the same as though it had been
cloned directly from a fetch URL.</p>
</div>

</div>
EOT
