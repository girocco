#!/bin/sh

# Usage: backup-db.sh [--all]

# This script makes a rotating backup of the very
# important database files $chroot/etc/passwd and
# $chroot/etc/group and $chroot/etc/sshkeys.
# Backup files are rotated dropping .9.$ext and
# renaming the others to the next higher number
# and then finally making a copy of the original
# to a .1 (not compressed for passwd and group).

# If the `--all` option is specified, then project
# metadata for each project is also backed up (all
# the project's settings -- pretty much everything
# except the objects from the repository).

# The backup files are stored in $chroot/etc/backups/
# which MUST ALREADY EXIST (jailsetup.sh creates it).

# Running this regularly from a cron entry is
# highly recommended.  It may be run manually
# from the $basedir/toolbox directory any time
# after make install has been run to create an
# immediate backup.

# Backups retain the modification date of the file
# they are backing up at the time the backup was
# made.  If restoration is required this can be
# used to determine the time period from which data
# would be lost if the backup were to be used.

# Backups are moved into place after being fully
# created with a ".tmp_" prefix and are therefore
# always safe to copy out once they have their
# final backup name (i.e. the leading ".tmp_" has
# been removed).

set -e

. @basedir@/shlib.sh

# returns specified hash (e.g. "md5", "sha1" etc.)
#   $1 => variable name to store result in
#   $2 => hash name ("md5", "sha1" or anything openssl dgst understands)
#   $3 => file to hash
vgethash()
{
	eval "$1="
	_hash="$('openssl' dgst -"$2" <"$3" 2>/dev/null)" &&
	_hash="${_hash##*[!0-9a-fA-F]}" && [ -n "$_hash" ] &&
	eval "$1=\"$_hash\""
}

# rotate_file basename suffix
rotate_file2_9() {
	! [ -f "$1.8.$2" ] || mv -f "$1.8.$2" "$1.9.$2"
	! [ -f "$1.7.$2" ] || mv -f "$1.7.$2" "$1.8.$2"
	! [ -f "$1.6.$2" ] || mv -f "$1.6.$2" "$1.7.$2"
	! [ -f "$1.5.$2" ] || mv -f "$1.5.$2" "$1.6.$2"
	! [ -f "$1.4.$2" ] || mv -f "$1.4.$2" "$1.5.$2"
	! [ -f "$1.3.$2" ] || mv -f "$1.3.$2" "$1.4.$2"
	! [ -f "$1.2.$2" ] || mv -f "$1.2.$2" "$1.3.$2"
	return 0
}

# backup_file dir basename backupdir
backup_file() {
	rotate_file2_9 "$3/$2" gz
	if [ -f "$3/$2.1" ]; then
		rm -f "$3/$2.2.gz" "$3/.tmp_$2.2.gz" &&
		gzip -n9 <"$3/$2.1" >"$3/.tmp_$2.2.gz" &&
		touch -r "$3/$2.1" "$3/.tmp_$2.2.gz" &&
		mv -f "$3/.tmp_$2.2.gz" "$3/$2.2.gz" &&
		chmod a-w "$3/$2.2.gz"
	fi
	if [ -f "$1/$2" ]; then
		rm -f "$3/$2.1" "$3/.tmp_$2.1" &&
		cp -pf "$1/$2" "$3/.tmp_$2.1" &&
		mv -f "$3/.tmp_$2.1" "$3/$2.1" &&
		chmod a-w "$3/$2.1"
	fi
	return 0
}

# badkup_dir dir basename backupdir
backup_dir() (
	set -e
	rotate_file2_9 "$3/$2" tar.gz
	! [ -f "$3/$2.1.tar.gz" ] || mv -f "$3/$2.1.tar.gz" "$3/$2.2.tar.gz"
	if [ -d "$1/$2" ]; then
		cd "$1" &&
		rm -f "$3/$2.1.tar.gz" "$3/.tmp_$2.1.tar.gz" &&
		tar -c -f - "$2" | gzip -n9 >"$3/.tmp_$2.1.tar.gz" &&
		mv -f "$3/.tmp_$2.1.tar.gz" "$3/$2.1.tar.gz" &&
		chmod a-w "$3/$2.1.tar.gz"
	fi
	return 0
)

backup_projects() (
	set -e
	bombin="$cfg_basedir/toolbox/create_projects_bom.pl"
	[ -f "$bombin" ] && [ -x "$bombin" ] || {
		echo 'ERROR: backup-db.sh could not find create_projects_bom.pl' >&2
		echo 'ERROR: backup-db.sh projects data NOT backed up!' >&2
		return
	}
	cpiobin="$cfg_basedir/toolbox/perlcpio.pl"
	[ -f "$cpiobin" ] && [ -x "$cpiobin" ] || {
		echo 'ERROR: backup-db.sh could not find perlcpio.pl' >&2
		echo 'ERROR: backup-db.sh projects data NOT backed up!' >&2
		return
	}
	[ -d "$1/$2" ] || {
		echo "ERROR: no such directory: $1/$2" >&2
		echo 'ERROR: backup-db.sh projects data NOT backed up!' >&2
		return
	}
	rotate_file2_9 "$3/$2" pax.gz
	rotate_file2_9 "$3/$2" pax.gz.sha1
	! [ -f "$3/$2.1.pax.gz" ] || mv -f "$3/$2.1.pax.gz" "$3/$2.2.pax.gz"
	! [ -f "$3/$2.1.pax.gz.sha1" ] || mv -f "$3/$2.1.pax.gz.sha1" "$3/$2.2.pax.gz.sha1"
	cd "$1" &&
	rm -f "$3/$2.1.pax.gz" "$3/.tmp_$2.1.pax.gz" &&
	"$bombin" | sed "s,^,./$2/," |
	"$cpiobin" ${var_cgi_uid:+-u} $var_cgi_uid ${var_group_gid:+-g} $var_group_gid -kcq |
	gzip -n9 >"$3/.tmp_$2.1.pax.gz" &&
	mv "$3/.tmp_$2.1.pax.gz" "$3/$2.1.pax.gz" &&
	chmod a-w "$3/$2.1.pax.gz" &&
	if vgethash sha1 sha1 "$3/$2.1.pax.gz"; then
		rm -f "$3/$2.1.pax.gz.sha1" "$3/.tmp_$2.1.pax.gz.sha1" &&
		echo "$sha1" >"$3/.tmp_$2.1.pax.gz.sha1" &&
		touch -r "$3/$2.1.pax.gz" "$3/.tmp_$2.1.pax.gz.sha1" &&
		mv "$3/.tmp_$2.1.pax.gz.sha1" "$3/$2.1.pax.gz.sha1" &&
		chmod a-w "$3/$2.1.pax.gz.sha1"
	fi
)

# Be paranoid
if [ -z "$cfg_chroot" ] || [ "$cfg_chroot" = "/" ]; then
  echo 'Config.pm chroot setting is invalid' >&2
  exit 1
fi
if [ ! -d "$cfg_chroot/etc/backups" ]; then
  echo "No such directory: $cfg_chroot/etc/backups" >&2
  exit 1
fi
if [ ! -w "$cfg_chroot/etc/backups" ]; then
  echo "Not writable directory: $cfg_chroot/etc/backups" >&2
  exit 1
fi

tmploc=

backup_file "$cfg_chroot/etc" "passwd" "$cfg_chroot/etc/backups"
backup_file "$cfg_chroot/etc" "group" "$cfg_chroot/etc/backups"
backup_dir "$cfg_chroot/etc" "sshkeys" "$cfg_chroot/etc/backups"
[ "$1" != "--all" ] || {
	! command -v renice >/dev/null 2>&1 || renice -n +10 -p $$ >/dev/null 2>&1 || :
	! command -v ionice >/dev/null 2>&1 || ionice -c 3 -p $$ >/dev/null 2>&1 || :
	tmploc="$(mktemp -d /tmp/backup-db-XXXXXX)"
	ln -s "$cfg_reporoot" "$tmploc/projects"
	backup_projects "$tmploc" "projects" "$cfg_chroot/etc/backups"
}

[ -z "$tmploc" ] || rm -rf "$tmploc"

exit 0
