#!/bin/sh

set -e

if [ $# -ne 3 ] || [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
	echo "Usage: $0 <username> <old e-mail> <new e-mail>"
	exit 1
fi
echo "Redirecting to usertool.pl..."
exec @basedir@/toolbox/usertool.pl setemail "$1" "$3" "$2"
