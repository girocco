#!/bin/sh

set -e

if [ $# -ne 1 ] || [ -z "$1" ]; then
	echo "Usage: $0 <username>"
	exit 1
fi
echo "Redirecting to usertool.pl..."
exec @basedir@/toolbox/usertool.pl remove "$1"
