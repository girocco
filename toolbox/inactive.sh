#!/bin/sh

echo "Not ready for usage yet; have to figure out locking and check conditions --jast"
exit 99

. @basedir@/shlib.sh

echo Checking for inactive projects (empty repositories)

cd "$cfg_reporoot"
mkdir -p "$cfg_reporoot/_recyclebin"
for i in $(find -L . -name heads -type d); do [ "$(ls $i)" ] && continue; fb="${i##*/}"; fb="${fb%.git}"; [ -d "$i/../../../$fb" ] && continue; [ -e "$i/../../packed-refs" ] && continue; echo "${i%/refs/heads} $(cat $i/../../owner)"; done >sack

while read x y; do echo -e 'Hello,\n\nI have removed your project '"$x from $cfg_name since you have set it up quite some time ago and nothing has ever been pushed in (I run this job once in few months). If you want to publish anything on $cfg_name in the future, feel free to create the project again. If you experienced any technical difficulties with publishing on $cfg_name, please let me know, hopefully we can solve them together (sometimes I don't reply for some time, in that case it's best to harass me again, and repeatedly ;-). In case the automated check went wrong, the repository is still backed up..."'\n\nThanks for using '"$cfg_name"'!\n\n\t\t\tPetr Baudis <pasky@suse.cz>' | mail -s "[$cfg_name] Automated project removal" $y; done <sack

#for i in $(find -L . -name heads -type d); do [ "$(ls $i)" ] && continue; mv ${i%/refs/heads} "$cfg_reporoot/_recyclebin/"; done
while read x y; do f="${x%/*}"; x="${x##*/}"; [ "$f" != "$x" ] || f=""; echo "$f .. $x"; mkdir -p "$cfg_reporoot/_recyclebin/$f"; mv "$cfg_reporoot/$f/$x" "$cfg_reporoot/_recyclebin/$f"; done <sack

for i in `cut -d ' ' -f 1 "$cfg_reporoot/sack"`; do echo $i; j=${i#./}; j=${j%.git}; perl -Icgi -MGirocco::Project -e 'Girocco::Project->ghost("'$j'", 0)->_group_remove()'; done
