#!/usr/bin/perl

# lint-all-readme.pl - lint all projects' explicit README files
# Copyright (C) 2021 Kyle J. McKay.
# All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;
use vars qw($VERSION);
BEGIN {*VERSION = \'1.0.1'}
use File::Basename qw(basename);
use Cwd qw(realpath);
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::Util;
use Girocco::CLIUtil;
use Girocco::Project;
my $bn; BEGIN {$bn = basename(__FILE__)}

exit(&main(@ARGV)||0);

our $help;
BEGIN {$help = <<'HELP'}
Usage: %s [--help] [--dry-run] [<projname>]...
       --help           show this help
       --dry-run        show projects that need updating but don't update them
       -P/--progress    show progress on STDERR (default if STDERR is a tty)
       -q/--quiet       suppress warning messages

       <projname>...    if given, only operate on these projects

       Exit status will always be non-zero if any readme files fail to lint.
HELP

sub lint_project_readmes {
	my $dryrun = shift;
	my $show_progress = shift;
	my $quiet = shift;
	my %allprojs = map({$_ => 1} Girocco::Project::get_full_list());
	my @projs = ();
	if (@_) {
		my $root = $Girocco::Config::reporoot;
		$root =~ s,/+$,,;
		$root ne "" or $root = "/";
		$root = realpath($root);
		$root = $1 if $root =~ m|^(/.+)$|;
		my %seen = ();
		foreach (@_) {
			exists($seen{$_}) and next;
			if (!exists($allprojs{$_})) {
				s,/+$,,;
				$_ ne "" or $_ = "/";
				-d $_ and $_ = realpath($_);
				s,^\Q$root\E/,,;
				s,^/+,,;
				$_ = $1 if $_ =~ m|^(.+)$|;
				s,\.git$,,;
				exists($seen{$_}) and next;
				if (!exists($allprojs{$_})) {
					$seen{$_} = 1;
					warn "$_: unknown to Girocco (not in etc/group)\n"
						unless $quiet;
					next;
				}
			}
			$seen{$_} = 1;
			push(@projs, $_);
		}
	} else {
		@projs = sort({lc($a) cmp lc($b) || $a cmp $b} keys(%allprojs));
	}
	my @outdated = ();
	my @badlint = ();
	my $bd = $Girocco::Config::reporoot . '/';
	my $progress = Girocco::CLIUtil::Progress->new(
		$show_progress ? scalar(@projs) : 0,
		"Checking project readme files");
	my $cnt = 0;
	foreach (@projs) {
		++$cnt;
		$progress->update($cnt);
		my $pd = $bd . $_ . '.git';
		-d $pd or next; # just ignore any phantoms
		my $proj = undef;
		eval { $proj = Girocco::Project->load($_); 1; } && $proj or
			next; # just ignore unloadable projects
		my $readme = $proj->{README};
		defined($readme) or $readme = "";
		chomp($readme);
		my ($cnt, $err) = $proj->_lint_readme(0);
		if ($cnt) {
			push(@badlint, $_);
			$progress->emit("$_: error: $err");
			next;
		}
		my $newreadme = $proj->{README};
		defined($newreadme) or $newreadme = "";
		chomp($newreadme);
		$readme eq $newreadme and next;
		if ($dryrun) {
			push(@outdated, $_);
			$progress->emit("$_: needs update");
		} else {
			push(@outdated, $_);
			$proj->_property_fput("READMEDATA", $proj->{READMEDATA}, 1);
			$proj->_property_fput("README", $proj->{README}, -e "$pd/README.html");
			$proj->_property_fput("rmtype", $proj->{rmtype}, 1);
			$proj->_set_changed;
			$progress->emit("$_: updated");
		}
	}
	return {count => scalar(@projs), outdated => \@outdated,
		badlint => \@badlint};
}

sub dohelp {
	my $fd = shift;
	my $ec = shift;
	printf $fd "%s version %s\n", $bn, $VERSION;
	printf $fd $help, $bn;
	exit $ec;
}

sub main {
	local *ARGV = \@_;
	my ($quiet, $dryrun, $help);
	my $progress = -t STDERR;
	{
		shift, $quiet=1, redo if @ARGV && $ARGV[0] =~ /^(?:-q|--quiet)$/i;
		shift, $dryrun=1, redo if @ARGV && $ARGV[0] =~ /^(?:-n|--dry-run)$/i;
		shift, $help=1, redo if @ARGV && $ARGV[0] =~ /^(?:-h|--help)$/i;
		shift, $progress=1, redo if @ARGV && $ARGV[0] =~ /^(?:-P|--progress)$/i;
		shift, $progress=0, redo if @ARGV && $ARGV[0] =~ /^(?:--no-progress)$/i;
	}
	$help || (@ARGV && $ARGV[0] =~ /^-/) and dohelp($help ? \*STDOUT : \*STDERR, !$help);
	nice_me(18);
	my $results = lint_project_readmes(!!$dryrun, $progress, $quiet, @ARGV);
	printf "Total: %d  %s: %d  Lintfail: %d\n",
		$results->{count},
		$dryrun ? "Outdated" : "Updated", scalar(@{$results->{outdated}}),
		scalar(@{$results->{badlint}});
	exit @{$results->{badlint}} ? 1 : 0;
}
