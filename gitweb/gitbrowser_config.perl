## For the complete overview of available configuration options,
## see git-browser.git/git-browser.cgi file right after "package inner" line

# Pull Girocco config
use lib "__BASEDIR__";
use Girocco::Config;

# If $check_path is set to a subroutine reference, it will be called
# by get_repo_path with two arguments, the name of the repo and its
# path which will be undef if it's not a known repo.  If the function
# returns false, access to the repo will be denied.
# $check_path = sub { my ($name, $path) = @_; $name ~! /restricted/i; };
our $check_path = sub {
	my ($name, $path) = @_;
	if (not defined $path and
		$name =~ m!^/*_! ||
		$name =~ m!\.\.! ||
		!($name =~ m!\.git/*$!) ||
		$name =~ m!\.git/.*\.git/*$!i) {
		return undef;
	}
	return 1;
};

# Since we actually want to display "replaced" commits in
# git-browser just the same as gitweb does, make sure that
# GIT_NO_REPLACE_OBJECTS is NOT set in the environment for git-browser.
delete $ENV{'GIT_NO_REPLACE_OBJECTS'};

# This should not be needed, but just in case, make the last expression true
1;
