/* LibTomCrypt, modular cryptographic library -- Tom St Denis
 *
 * LibTomCrypt is a library that provides various cryptographic
 * algorithms in a highly modular and flexible manner.
 *
 * The library is free for all purposes without any express
 * guarantee it works.
 *
 * License: public domain -or- http://www.wtfpl.net/txt/copying/
 */

/**
  @file sha1.c
  LTC_SHA1 code by Tom St Denis
*/

#include "lt1.h"
#include <stdlib.h>
#include <string.h>

#ifndef MIN
  #define MIN(x, y) ( ((x)<(y))?(x):(y) )
#endif

#define CRYPT_OK 1
#define hash_state SHA1_CTX
#define sha1_init SHA1_Init
#define sha1_process SHA1_Update
#define sha1_done SHA1_Final

#define STORE32H(x, y)                                                                     \
  do { (y)[0] = (unsigned char)(((x)>>24)&255); (y)[1] = (unsigned char)(((x)>>16)&255);   \
       (y)[2] = (unsigned char)(((x)>>8)&255); (y)[3] = (unsigned char)((x)&255); } while(0)

#define LOAD32H(x, y)                       \
  do { x = ((uint32_t)((y)[0] & 255)<<24) | \
           ((uint32_t)((y)[1] & 255)<<16) | \
           ((uint32_t)((y)[2] & 255)<<8)  | \
           ((uint32_t)((y)[3] & 255)); } while(0)

#define STORE64H(x, y)                                                                     \
do { (y)[0] = (unsigned char)(((x)>>56)&255); (y)[1] = (unsigned char)(((x)>>48)&255);     \
     (y)[2] = (unsigned char)(((x)>>40)&255); (y)[3] = (unsigned char)(((x)>>32)&255);     \
     (y)[4] = (unsigned char)(((x)>>24)&255); (y)[5] = (unsigned char)(((x)>>16)&255);     \
     (y)[6] = (unsigned char)(((x)>>8)&255); (y)[7] = (unsigned char)((x)&255); } while(0)

/* 32-bit Rotates */
#if defined(_MSC_VER)
#define LTC_ROx_BUILTIN

/* instrinsic rotate */
#include <stdlib.h>
#pragma intrinsic(_rotr,_rotl)
#define ROL(x,n) _rotl(x,n)
#define ROLc(x,n) ROL(x,n)

#elif defined(LTC_HAVE_ROTATE_BUILTIN)
#define LTC_ROx_BUILTIN

#define ROL(x,n) __builtin_rotateleft32(x,n)
#define ROLc(x,n) ROL(x,n)

#elif !defined(__STRICT_ANSI__) && defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__)) && !defined(INTEL_CC) && !defined(LTC_NO_ASM)
#define LTC_ROx_ASM

static inline uint32_t ROL(uint32_t word, int i)
{
   asm ("roll %%cl,%0"
      :"=r" (word)
      :"0" (word),"c" (i));
   return word;
}

#ifndef LTC_NO_ROLC

#define ROLc(word,i) ({ \
   uint32_t ROLc_tmp = (word); \
   __asm__ ("roll %2, %0" : \
            "=r" (ROLc_tmp) : \
            "0" (ROLc_tmp), \
            "I" (i)); \
            ROLc_tmp; \
   })

#else

#define ROLc ROL

#endif

#elif !defined(__STRICT_ANSI__) && defined(LTC_PPC32)
#define LTC_ROx_ASM

static inline uint32_t ROL(uint32_t word, int i)
{
   asm ("rotlw %0,%0,%2"
      :"=r" (word)
      :"0" (word),"r" (i));
   return word;
}

#ifndef LTC_NO_ROLC

static inline uint32_t ROLc(uint32_t word, const int i)
{
   asm ("rotlwi %0,%0,%2"
      :"=r" (word)
      :"0" (word),"I" (i));
   return word;
}

#else

#define ROLc ROL

#endif

#else

/* rotates the hard way */
#define ROL(x, y) ( (((uint32_t)(x)<<(uint32_t)((y)&31)) | (((uint32_t)(x)&0xFFFFFFFFUL)>>(uint32_t)((32-((y)&31))&31))) & 0xFFFFFFFFUL)
#define ROLc(x, y) ( (((uint32_t)(x)<<(uint32_t)((y)&31)) | (((uint32_t)(x)&0xFFFFFFFFUL)>>(uint32_t)((32-((y)&31))&31))) & 0xFFFFFFFFUL)

#endif

/* a simple macro for making hash "process" functions */
#define HASH_PROCESS(func_name, compress_name, block_size)                                  \
int func_name(hash_state *md, const void *_in, size_t inlen)                                \
{                                                                                           \
    const unsigned char *in = (const unsigned char *)_in;                                   \
    size_t        n;                                                                        \
    int           err;                                                                      \
    if (!md || !in) return 0;                                                               \
    if (md->curlen > sizeof(md->buf)) {                                                     \
       return 0;                                                                            \
    }                                                                                       \
    if ((md->length + inlen) < md->length) {                                                \
      return 0;                                                                             \
    }                                                                                       \
    while (inlen > 0) {                                                                     \
        if (md->curlen == 0 && inlen >= block_size) {                                       \
           if ((err = compress_name(md, in)) != CRYPT_OK) {                                 \
              return err;                                                                   \
           }                                                                                \
           md->length += block_size * 8;                                                    \
           in             += block_size;                                                    \
           inlen          -= block_size;                                                    \
        } else {                                                                            \
           n = MIN(inlen, (block_size - md->curlen));                                       \
           memcpy(md->buf + md->curlen, in, (size_t)n);                                     \
           md->curlen += n;                                                                 \
           in             += n;                                                             \
           inlen          -= n;                                                             \
           if (md->curlen == block_size) {                                                  \
              if ((err = compress_name(md, md->buf)) != CRYPT_OK) {                         \
                 return err;                                                                \
              }                                                                             \
              md->length += 8*block_size;                                                   \
              md->curlen = 0;                                                               \
           }                                                                                \
       }                                                                                    \
    }                                                                                       \
    return CRYPT_OK;                                                                        \
}

#define F0(x,y,z)  (z ^ (x & (y ^ z)))
#define F1(x,y,z)  (x ^ y ^ z)
#define F2(x,y,z)  ((x & y) | (z & (x | y)))
#define F3(x,y,z)  (x ^ y ^ z)

static int sha1_compress(hash_state *md, const unsigned char *buf)
{
    uint32_t a,b,c,d,e,W[80],i;
#ifdef LTC_SMALL_CODE
    uint32_t t;
#endif

    /* copy the state into 512-bits into W[0..15] */
    for (i = 0; i < 16; i++) {
        LOAD32H(W[i], buf + (4*i));
    }

    /* copy state */
    a = md->state[0];
    b = md->state[1];
    c = md->state[2];
    d = md->state[3];
    e = md->state[4];

    /* expand it */
    for (i = 16; i < 80; i++) {
        W[i] = ROL(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
    }

    /* compress */
    /* round one */
    #define FF0(a,b,c,d,e,i) e = (ROLc(a, 5) + F0(b,c,d) + e + W[i] + 0x5a827999UL); b = ROLc(b, 30);
    #define FF1(a,b,c,d,e,i) e = (ROLc(a, 5) + F1(b,c,d) + e + W[i] + 0x6ed9eba1UL); b = ROLc(b, 30);
    #define FF2(a,b,c,d,e,i) e = (ROLc(a, 5) + F2(b,c,d) + e + W[i] + 0x8f1bbcdcUL); b = ROLc(b, 30);
    #define FF3(a,b,c,d,e,i) e = (ROLc(a, 5) + F3(b,c,d) + e + W[i] + 0xca62c1d6UL); b = ROLc(b, 30);

#ifdef LTC_SMALL_CODE

    for (i = 0; i < 20; ) {
       FF0(a,b,c,d,e,i++); t = e; e = d; d = c; c = b; b = a; a = t;
    }

    for (; i < 40; ) {
       FF1(a,b,c,d,e,i++); t = e; e = d; d = c; c = b; b = a; a = t;
    }

    for (; i < 60; ) {
       FF2(a,b,c,d,e,i++); t = e; e = d; d = c; c = b; b = a; a = t;
    }

    for (; i < 80; ) {
       FF3(a,b,c,d,e,i++); t = e; e = d; d = c; c = b; b = a; a = t;
    }

#else

    for (i = 0; i < 20; ) {
       FF0(a,b,c,d,e,i++);
       FF0(e,a,b,c,d,i++);
       FF0(d,e,a,b,c,i++);
       FF0(c,d,e,a,b,i++);
       FF0(b,c,d,e,a,i++);
    }

    /* round two */
    for (; i < 40; )  {
       FF1(a,b,c,d,e,i++);
       FF1(e,a,b,c,d,i++);
       FF1(d,e,a,b,c,i++);
       FF1(c,d,e,a,b,i++);
       FF1(b,c,d,e,a,i++);
    }

    /* round three */
    for (; i < 60; )  {
       FF2(a,b,c,d,e,i++);
       FF2(e,a,b,c,d,i++);
       FF2(d,e,a,b,c,i++);
       FF2(c,d,e,a,b,i++);
       FF2(b,c,d,e,a,i++);
    }

    /* round four */
    for (; i < 80; )  {
       FF3(a,b,c,d,e,i++);
       FF3(e,a,b,c,d,i++);
       FF3(d,e,a,b,c,i++);
       FF3(c,d,e,a,b,i++);
       FF3(b,c,d,e,a,i++);
    }
#endif

    #undef FF0
    #undef FF1
    #undef FF2
    #undef FF3

    /* store */
    md->state[0] = md->state[0] + a;
    md->state[1] = md->state[1] + b;
    md->state[2] = md->state[2] + c;
    md->state[3] = md->state[3] + d;
    md->state[4] = md->state[4] + e;

    return CRYPT_OK;
}

/**
   Initialize the hash state
   @param md   The hash state you wish to initialize
   @return CRYPT_OK if successful
*/
int sha1_init(hash_state *md)
{
   if (!md) return 0;

   md->curlen = 0;
   md->length = 0;
   md->state[0] = 0x67452301UL;
   md->state[1] = 0xefcdab89UL;
   md->state[2] = 0x98badcfeUL;
   md->state[3] = 0x10325476UL;
   md->state[4] = 0xc3d2e1f0UL;
   return CRYPT_OK;
}

/**
   Process a block of memory though the hash
   @param md     The hash state
   @param in     The data to hash
   @param inlen  The length of the data (octets)
   @return CRYPT_OK if successful
*/
HASH_PROCESS(sha1_process, sha1_compress, 64)

/**
   Terminate the hash to get the digest
   @param md  The hash state
   @param out [out] The destination of the hash (20 bytes)
   @return CRYPT_OK if successful
*/
int sha1_done(unsigned char *out, hash_state *md)
{
    int i;

    if (!md || !out) return 0;

    if (md->curlen >= sizeof(md->buf)) {
       return 0;
    }

    /* increase the length of the message */
    md->length += md->curlen * 8;

    /* append the '1' bit */
    md->buf[md->curlen++] = (unsigned char)0x80;

    /* if the length is currently above 56 bytes we append zeros
     * then compress.  Then we can fall back to padding zeros and length
     * encoding like normal.
     */
    if (md->curlen > 56) {
        while (md->curlen < 64) {
            md->buf[md->curlen++] = (unsigned char)0;
        }
        sha1_compress(md, md->buf);
        md->curlen = 0;
    }

    /* pad upto 56 bytes of zeroes */
    while (md->curlen < 56) {
        md->buf[md->curlen++] = (unsigned char)0;
    }

    /* store length */
    STORE64H(md->length, md->buf+56);
    sha1_compress(md, md->buf);

    /* copy output */
    for (i = 0; i < 5; i++) {
        STORE32H(md->state[i], out+(4*i));
    }
    return CRYPT_OK;
}

unsigned char *SHA1(const void *data, size_t len, unsigned char *md)
{
    SHA1_CTX c;

    if (!SHA1_Init(&c)) return NULL;
    if (!SHA1_Update(&c, data, len)) return NULL;
    if (!SHA1_Final(md, &c)) return NULL;
    return md;
}
