# These entries should be ADDED to the end of /etc/fstab to
# properly set up the girocco chroot jail

# /etc/fstab: static file system information.
#
# <file system>	<mount point>		<type>	<options>		<dump>	<pass>

# IMPORTANT:
#  It may be necessary to add a "late" option to these entries
#  so that they are mounted after the filesystem they are trying
#  to mount into (such as ZFS).  On FreeBSD this can be accomplished
#  simply by adding a "late" option.  On Linux if delaying the mounts
#  is required, it may be necessary to add a "noauto" option and then
#  manually mount them in /etc/rc.local.

# mount the git repositories in the jail
#   Note that on FreeBSD a "bind" mount is accomplished by setting the type to
#     nullfs and the options to rw as FreeBSD does not have a mount --bind option
#   Note that DragonFly BSD is the same as FreeBSD except the type should be null
/srv/git	/home/repo/j/srv/git	none	bind			0	0

# mount the proc filesystem in the jail
#   Note that there are two possiblities here, the first is a bind mount and
#     the second is another proc mount.
#   The bind mount will always mirror everything in /proc whereas a second
#     proc mount has the potential of having a separate proc namespace.
#   Note that some systems (e.g. FreeBSD) must omit the "nodev" option.
#   Note that some systems (e.g. FreeBSD) will need to use "procfs" for the type.
#   Note that some systems (e.g. FreeBSD) may need to add the "rw" option.
#   Note that mounting procfs in a FreeBSD chroot is optional and normally omitted
#/proc		/home/repo/j/proc	none	bind			0	0
proc		/home/repo/j/proc	proc	nodev,noexec,nosuid	0	0

# mount the devfs filesystem in the jail
#   Note that this is only necessary on systems where mknod does not work
#     except on devfs mounts (e.g. FreeBSD, DragonFly BSD)
#   Note that on FreeBSD before starting the chroot, the following commands
#     may be executed to reduce the number of exposed devices:
#       devfs -m /home/repo/j/dev ruleset 4
#       devfs -m /home/repo/j/dev rule applyset
#   Note that on DragonFly BSD before starting the chroot, the devsfctl
#     commands shown in the devfsctl_chroot_ruleset file (which will be
#     automatically installed to $Girocco::Config::chroot/etc/devfs.conf)
#     may be executed to reduce the number of devices exposed in the chroot.
#devfs		/home/repo/j/dev	devfs	rw,noexec,nosuid	0	0
